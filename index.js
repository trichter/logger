const colors = require('colors')

colors.setTheme({
    silly: 'rainbow',
    input: 'grey',
    verbose: 'cyan',
    prompt: 'grey',
    info: 'green',
    data: 'grey',
    help: 'cyan',
    warn: 'yellow',
    debug: 'blue',
    error: 'red'
})
   

function log(namespace, level) {
    return function(message, ...args) {
        let date = new Date()
        if(message instanceof Error) {
            args.unshift(message.stack)
            message = message.message
        }

        let colorizedLevel = level[level] || level
        let str = `${date.toLocaleTimeString()} ${namespace.bold} ${colorizedLevel} ${message}`
        if(console[level]) {
            console[level](str, ...args)
        } else {
            console.log(str, ...args)
        }
    }
}

const debugEnabled = process.env.DEBUG === "1" || (process.env.NODE_ENV !== 'production' && !process.env.DEBUG)

module.exports = function (namespace) {
    return {
        debug: debugEnabled ? log(namespace, 'debug') : function() {},
        info: log(namespace, 'info'),
        warn: log(namespace, 'warn'),
        error: log(namespace, 'error'),
    }
}
module.exports.default = module.exports