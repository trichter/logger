export default function (namespace: string): {
    debug: (message: string | Error, ...args: any[]) => void;
    info: (message: string | Error, ...args: any[]) => void;
    warn: (message: string | Error, ...args: any[]) => void;
    error: (message: string | Error, ...args: any[]) => void;
};
